import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { KeycloakService } from './app/services/keycloak/keycloak.service';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}
// Réactivation de keycloack mettre en commentaire les deuxs lignes suivante

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

// Réactivation de keycloack enlever commentaire

  /*
KeycloakService.init()
  .then(() => platformBrowserDynamic().bootstrapModule(AppModule))
  .catch(e => window.location.reload());
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
*/ 

//