import { Component, OnInit } from '@angular/core';
import { dashboard } from './dashboard';

@Component({
  selector: 'app-flux',
  templateUrl: './flux.component.html',
  styleUrls: ['./flux.component.css']
})
export class FluxComponent implements OnInit {
selectedTechno : dashboard;
selectedPoste : dashboard;
selectedLocalisation : dashboard;
selectedClient : dashboard;


  constructor() { }
 
  ngOnInit() {
  }
  chartOptions = {
    responsive: true
  };

  chartData = [
    { data: [330, 600, 260, 700], label: 'Client 1' },
    { data: [120, 455, 100, 340], label: 'Client 2' },
    { data: [45, 67, 800, 500], label: 'Client 3' }
  ];

  chartLabels = ['January', 'February', 'Mars', 'April'];

  onChartClick(event) {
    console.log(event);
  }
  newDataPoint(dataArr = [100, 100, 100], label) {

    this.chartData.forEach((dataset, index) => {
      this.chartData[index] = Object.assign({}, this.chartData[index], {
        data: [...this.chartData[index].data, dataArr[index]]
      });
    });
  
    this.chartLabels = [...this.chartLabels, label];
  
  }
  charColor = [
    {
      backgroundColor: 'rgba(103, 58, 183, .1)',
      borderColor: 'rgb(103, 58, 183)',
      pointBackgroundColor: 'rgb(103, 58, 183)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(103, 58, 183, .8)'
    },
  ];
  onSelect(dashboard: dashboard): void {
   
    this.selectedTechno = dashboard;
    this.selectedPoste = dashboard;
    this.selectedLocalisation = dashboard;
    this.selectedClient= dashboard;

  

  }
  
}
