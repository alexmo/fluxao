import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxDetailsComponent } from './flux-details.component';

describe('FluxDetailsComponent', () => {
  let component: FluxDetailsComponent;
  let fixture: ComponentFixture<FluxDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FluxDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
