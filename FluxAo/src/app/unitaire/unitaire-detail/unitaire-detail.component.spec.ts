import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitaireDetailComponent } from './unitaire-detail.component';

describe('UnitaireDetailComponent', () => {
  let component: UnitaireDetailComponent;
  let fixture: ComponentFixture<UnitaireDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitaireDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitaireDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
