import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitaireComponent } from './unitaire.component';

describe('UnitaireComponent', () => {
  let component: UnitaireComponent;
  let fixture: ComponentFixture<UnitaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
