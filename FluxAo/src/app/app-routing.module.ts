import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { HttpClientModule } from '@angular/common/http';
import { FluxComponent } from './flux/flux.component';
import { FluxDetailsComponent } from './flux/flux-details/flux-details.component';
import { UnitaireComponent } from './unitaire/unitaire.component';
import { UnitaireDetailComponent } from './unitaire/unitaire-detail/unitaire-detail.component';



const routes: Routes = [  
{ path: '', redirectTo: '/accueil', pathMatch: 'full' },
{ path: 'accueil', component: HomeComponent },
{ path: 'connexion', component: LoginFormComponent },
{ path: 'dashboard', component: FluxComponent },
{ path: 'fluxdetail', component: FluxComponent },
{ path: 'unitaire', component: UnitaireComponent },
{ path: 'unitaire-detail', component: UnitaireDetailComponent }// à modifier

];

@NgModule({
  imports: [RouterModule.forRoot(routes), HttpClientModule ],
  exports: [RouterModule]
})


export class AppRoutingModule { }
