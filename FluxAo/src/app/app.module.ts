import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER  } from '@angular/core';
import { HttpClientModule,HTTP_INTERCEPTORS  } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { UserComponent } from './user/user.component';
import { ChartsModule } from 'ng2-charts';
import { FluxComponent } from './flux/flux.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { DataService } from './services/data/data.service';
import { KeycloakService } from './services/keycloak/keycloak.service';
import { KeycloakInterceptorService } from './services/keycloak/keycloak.interceptor.service';
import { UnitaireComponent } from './unitaire/unitaire.component';
import { UnitaireDetailComponent } from './unitaire/unitaire-detail/unitaire-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LoginFormComponent,
    UserComponent,
    FluxComponent,
    DashboardComponent,
    UnitaireComponent,
    UnitaireDetailComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ChartsModule,
  
  ],
  providers: [    
    DataService,
    {
    provide: HTTP_INTERCEPTORS,
    useClass: KeycloakInterceptorService,
    multi: true
  },
  KeycloakService
],
  bootstrap: [AppComponent]
})
export class AppModule { }
